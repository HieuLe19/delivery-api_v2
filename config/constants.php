<?php
if (!defined('FIREBASE_CLOUD_MESSAGE_URL'))
    define('FIREBASE_CLOUD_MESSAGE_URL', 'https://fcm.googleapis.com/fcm/send');

if (!defined('IOS_DEVICE')) define('IOS_DEVICE', 1);
if (!defined('ANDROID_DEVICE')) define('ANDROID_DEVICE', 0);

if (!defined('ORDER_ALL_STATUS')) define('ORDER_ALL_STATUS', 4);

if (!defined('USER_ACTIVE_STATUS')) define('USER_ACTIVE_STATUS', 0);
if (!defined('USER_WARNING_STATUS')) define('USER_WARNING_STATUS', 1);
if (!defined('USER_PENDING_STATUS')) define('USER_PENDING_STATUS', 2);
if (!defined('USER_DISABLE_STATUS')) define('USER_DISABLE_STATUS', 3);
if (!defined('USER_ALL_STATUS')) define('USER_ALL_STATUS', 4);

if (!defined('USER_BAN_5_DAYS')) define('USER_BAN_5_DAYS', 0);

if (!defined('USER_IS_OFFLINE_STATUS')) define('USER_IS_OFFLINE_STATUS', 0);
if (!defined('USER_IS_ONLINE_STATUS')) define('USER_IS_ONLINE_STATUS', 1);
if (!defined('USER_IS_ALL_STATUS')) define('USER_IS_ALL_STATUS', 2);

if (!defined('NOTIFICATION_UNPUBLISH_STATUS')) define('NOTIFICATION_UNPUBLISH_STATUS', 0);
if (!defined('NOTIFICATION_PUBLISH_STATUS')) define('NOTIFICATION_PUBLISH_STATUS', 1);

if(!defined('PASSWORD_RANDOM_CHARS')) define('PASSWORD_RANDOM_CHARS', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
if(!defined('RANDOM_LENGTH')) define('RANDOM_LENGTH', 15);

if(!defined('USER_EARNING_DATE_TYPE')) define('USER_EARNING_DATE_TYPE', 1);
if(!defined('USER_EARNING_TURN_TYPE')) define('USER_EARNING_TURN_TYPE', 0);

if (!defined('BILL_IN_QUERY_STATUS')) define('BILL_IN_QUERY_STATUS', 0);
if (!defined('BILL_IN_PROCESS_STATUS')) define('BILL_IN_PROCESS_STATUS', 1);
if (!defined('BILL_DONE_STATUS')) define('BILL_DONE_STATUS', 2);
if (!defined('BILL_CANCEL_STATUS')) define('BILL_CANCEL_STATUS', 3);

if (!defined('BILL_HISTORY_UN_READ_STATUS')) define('BILL_HISTORY_UN_READ_STATUS', 0);
if (!defined('BILL_HISTORY_READ_STATUS')) define('BILL_HISTORY_READ_STATUS', 1);

if (!defined('VOUCHER_USED_STATUS')) define('VOUCHER_USED_STATUS', 1);
if (!defined('VOUCHER_IN_QUERY_STATUS')) define('VOUCHER_IN_QUERY_STATUS', 0);

if (!defined('MAX_RADIUS')) define('MAX_RADIUS', 10);

if (!defined('POINT_PLUSH_DISTRICT')) define('POINT_PLUSH_DISTRICT', 5);

