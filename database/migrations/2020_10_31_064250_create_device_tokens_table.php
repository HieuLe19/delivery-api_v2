<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDeviceTokensTable.
 */
class CreateDeviceTokensTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('device_tokens', function(Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->string('device_token', 255);
			$table->tinyInteger('type')->default(0)->comment('0: android; 1: ios');
			$table->text('uuid');
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('device_tokens');
	}
}
