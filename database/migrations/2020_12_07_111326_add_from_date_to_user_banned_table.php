<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFromDateToUserBannedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_banned', function (Blueprint $table) {
            $table->date('from_date')->after('comment')->nullable();
            $table->renameColumn('expire_date', 'to_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_banned', function (Blueprint $table) {
            $table->renameColumn('to_date', 'expire_date');
            $table->dropColumn('from_date');
        });
    }
}
