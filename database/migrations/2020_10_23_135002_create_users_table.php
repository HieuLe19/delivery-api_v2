<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDriversTable.
 */
class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
			$table->string('code', 100)->unique();
			$table->string('id_card', 20)->unique();
			$table->string('first_name', 255);
			$table->string('last_name', 255);
			$table->string('email', 100)->unique();
			$table->string('phone_number', 50)->unique();
			$table->string('password', 100);
			$table->dateTime('phone_verify_at')->nullable();
			$table->integer('reward_point')->default(0);
			$table->tinyInteger('status')->comment('0: active; 1: Warning_1; 2: Warning_2; 3: Inactive');
			$table->string('bank_account_number', 50);
			$table->string('bank_account_name', 255);
			$table->integer('bank_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('drivers');
	}
}
