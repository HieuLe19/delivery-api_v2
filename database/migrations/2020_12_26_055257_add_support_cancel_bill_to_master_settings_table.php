<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupportCancelBillToMasterSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_settings', function (Blueprint $table) {
            $table->float('support_cancel_bill')
                ->after('warning_point')
                ->default(0.2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_settings', function (Blueprint $table) {
            $table->dropColumn('support_cancel_bill');
        });
    }
}
