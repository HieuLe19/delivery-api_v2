<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->tinyInteger('status')
                ->default(0)->after('distance');
            $table->tinyInteger('payment_type')
                ->default(0)->after('distance');
            $table->text('comment')->after('distance');
            $table->float('amount')->after('distance');
            $table->integer('product_type_id')->after('distance');
            $table->date('finish_date')->after('distance');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('payment_type');
            $table->dropColumn('comment');
            $table->dropColumn('amount');
            $table->dropColumn('product_type_id');
            $table->dropColumn('finish_date');
        });
    }
}
