<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateOrdersTable.
 */
class CreateOrdersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code'); 
			$table->integer('bill_id');
			$table->tinyInteger('status')
				->comment('0:query; 1:receive; 2:inprocess;3:Done; 4:cancel')
				->default(0);
			$table->integer('payment_type')
				->comment('0:online; 1:offline')
				->default(1);
			$table->text('comment')->nullable();
			$table->float('amount')->default(0);
			$table->integer('product_type_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}
}
