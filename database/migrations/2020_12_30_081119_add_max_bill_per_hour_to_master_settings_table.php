<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxBillPerHourToMasterSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_settings', function (Blueprint $table) {
            $table->integer('max_bill_per_hour')
                ->after('support_cancel_bill')
                ->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_settings', function (Blueprint $table) {
            $table->dropColumn('max_bill_per_hour');
        });
    }
}
