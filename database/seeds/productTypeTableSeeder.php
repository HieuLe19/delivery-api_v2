<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class productTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('product_types')->insert([
            [
                'name' => 'văn phòng phẩm',
                'note' => 'Hàng dễ vỡ'
            ],
            [
                'name' => 'Hàng điện tử',
                'note' => 'Được phép xem hnh rũow'
            ],
        ]);
    }
}
