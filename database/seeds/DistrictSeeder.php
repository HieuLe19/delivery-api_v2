<?php

use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('district')->insert([
            ['name' => 'Quận 1'],
            ['name' => 'Quận 2'],
            ['name' => 'Quận 3'],
            ['name' => 'Quận 4'],
            ['name' => 'Quận 5'],
            ['name' => 'Quận 6'],
            ['name' => 'Quận 7'],
            ['name' => 'Quận 8'],
            ['name' => 'Quận 9'],
            ['name' => 'Quận 10'],
            ['name' => 'Quận 11'],
            ['name' => 'Quận 12'],
            ['name' => 'Quận Bình Tân'],
            ['name' => 'Quận Bình Thạnh'],
            ['name' => 'Quận Gò Vấp'],
            ['name' => 'Quận Phú Nhuận'],
            ['name' => 'Quận Tân Bình'],
            ['name' => 'Quận Tân Phú'],
            ['name' => 'Quận Thủ Đức'],
            ['name' => 'Huyện Bình Chánh'],
            ['name' => 'Huyện Cần Giờ'],
            ['name' => 'Huyện Củ Chi'],
            ['name' => 'Huyện Hóc Môn'],
            ['name' => 'Huyện Nhà Bè'],
        ]);
    }
}
