<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admins')->insert([
            [
                'name' => 'admin_1',
                'email' => 'superadmin@gmail.com',
                'password' => \Hash::make('123456'),
            ],
        ]);
    }
}
