<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MasterSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('master_settings')->insert([
            [
                'unit_price' => 10000, 
                'trade' => 0.2, 
                'warning_point' => 2,
            ],
        ]);
    }
}
