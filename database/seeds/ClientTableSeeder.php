<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('clients')->insert([
            [
                'name' => 'hieu_le', 
                'email' => 'hieu02@gmail.com', 
                'phone' => '0399823667',
                'password' => \Hash::make('123456')
            ],
        ]);
    }
}
