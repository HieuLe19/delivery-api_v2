<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            [
                'code' => 'AX-001',
                'id_card' => '301673608',
                'first_name' => 'Lê', 
                'last_name' => 'Minh Hiếu',
                'phone_number' => '0399823676',
                'reward_point' => 10,
                'status' => 0,
                'bank_account_number' => '123879187492',
                'bank_account_name' => "Lê Minh Hiếu",
                'bank_id' => 1,
                'email' => 'hieu01@gmail.com', 
                'password' => \Hash::make('123456')
            ],
        ]);
    }
}
