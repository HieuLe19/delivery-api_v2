<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\BillRepository;
use App\Repositories\MasterSettingRepository;
use App\Repositories\UserRepository;

class AssignBill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assignBill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign bill in query for user. Run each hours';

    protected $userRepository;
    protected $billRepository;
    protected $masterSettingRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        UserRepository $userRepository,
        BillRepository $billRepository,
        MasterSettingRepository $masterSettingRepository
    )
    {
        parent::__construct();
        $this->billRepository = $billRepository;
        $this->userRepository = $userRepository;
        $this->masterSettingRepository = $masterSettingRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bills = $this->billRepository->getUnAssignBill();
        foreach($bills as $bill) {
            $user = $this->pickUser($bill);
            if($user != null) {
                $this->billRepository->update(['user_id' => $user->id], $bill->id);
            }
        }
    }

    public function pickUser($bill)
    {
        $userResult = null;
        $point = 10000;
        $distance = 3;  //KM

        for($distance; $distance <= MAX_RADIUS; $distance++) {
            $users = $this->userRepository->byLocation(
                floatval($bill->latitude_from),
                floatval($bill->longitude_from), 
                $distance
            );
            if(count($users)) break;
        }
        foreach($users as $user) {
            // select user with minimum point
            $userPoint = $this->calculatePoint($user, $$bill->area);
            if($point > $userPoint) {
                $userResult = $user;
                $point = $userPoint;
            }
        }

        return $userResult;
    }

    public function calculatePoint($user, $district) 
    {
        $point = $user->reward_point;
        if($user->area_working == $district) {
            $point -= POINT_PLUSH_DISTRICT;
        }

        if($user->status == USER_WARNING_STATUS) {
            $masterData = $this->masterSettingRepository->first();
            $point += $masterData->warning_point;
        }

        return $point;
    }
}
