<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;
use App\Repositories\DeviceTokenRepository;

class TestPushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testPushNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $deviceTokenRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DeviceTokenRepository $deviceTokenRepository)
    {
        $this->deviceTokenRepository = $deviceTokenRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = 2;
        $devices = $this->deviceTokenRepository->findWhere(['user_id' => $userId]);
        $helper = new Helper();
        $data = [
            'title'     => 'TEST NOTIFICATION TITLE',
            'open_url'  => '',
            'body'      => 'Weekly points are renewed and become 20 points.',
        ];
        $helper->pushNotification($data, $devices);

    }
}
