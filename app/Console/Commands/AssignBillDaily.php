<?php

namespace App\Console\Commands;

use App\Repositories\BillRepository;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;

class AssignBillDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assignBillDaily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $billRepository;
    protected $userRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BillRepository $billRepository, UserRepository $userRepository)
    {
        $this->billRepository = $billRepository;
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bills = $this->billRepository->getUnAssignBill();
    }

    public function pickUserByDistrict($areaId, $bill)
    {

    }

    public function calculatePoint($user) 
    {
        $point = $user->reward_point;
        if($user->status == USER_WARNING_STATUS) {
            $masterData = $this->masterSettingRepository->first();
            $point += $masterData->warning_point;
        }

        return $point;
    }


}
