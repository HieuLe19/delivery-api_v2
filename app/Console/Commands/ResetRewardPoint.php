<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\UserRepository;

class ResetRewardPoint extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resetRewardPoint';

    protected $userRepository;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Reward point each month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->userRepository->all();

        foreach($users as $user) {
            $this->userRepository->update(['reward_point' => 0], $user->id);
        }
    }
}
