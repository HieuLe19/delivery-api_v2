<?php

namespace App\Console\Commands;

use App\Repositories\BillRepository;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PunishUserByBill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'punishUserByBill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $billRepository;
    protected $userRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        BillRepository $billRepository, 
        UserRepository $userRepository
    )
    {
        $this->billRepository = $billRepository;
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bills = $this->billRepository->getByOverDueFinishDate();

        foreach($bills as $bill) {
            try {
                $this->userRepository->update(
                    ['status' => USER_WARNING_STATUS], $bill->user_id);
            } catch(Exception $ex) {
                Log::error($ex);
            }
        }
    }
}
