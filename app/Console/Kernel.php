<?php

namespace App\Console;

use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AssignBill::class,
        Commands\AssignBillDaily::class,
        Commands\PunishUserByBill::class,
        Commands\ResetRewardPoint::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('assignBill')
                ->hourly();
        $schedule->command('assignBillDaily')
                ->dailyAt("0:00");
        $schedule->command('punishUserByBill')
                ->dailyAt('1:00');
        $schedule->command('resetRewardPoint')
                ->monthly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
