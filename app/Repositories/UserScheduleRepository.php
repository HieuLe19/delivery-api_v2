<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserScheduleRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserScheduleRepository extends RepositoryInterface
{
    //
}
