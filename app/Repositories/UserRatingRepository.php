<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRatingRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserRatingRepository extends RepositoryInterface
{
    //
}
