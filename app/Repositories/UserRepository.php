<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRatingRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    /**
     * Search user by condition 
     * @param array $conditions 
    */
    public function searchUser($conditions);
    public function byLocation($long, $lat, $distance);
    public function getListByDistrict($districtId);
}
