<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserBannedRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserBannedRepository extends RepositoryInterface
{
    //
}
