<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserEarningRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserEarningRepository extends RepositoryInterface
{
    public function sumOfMonth($userId);
}
