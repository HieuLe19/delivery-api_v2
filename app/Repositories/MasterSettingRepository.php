<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MasterSettingRepository.
 *
 * @package namespace App\Repositories;
 */
interface MasterSettingRepository extends RepositoryInterface
{
    //
}
