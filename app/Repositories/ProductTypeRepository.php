<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductTypeRepositoryRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProductTypeRepository extends RepositoryInterface
{
    //
}
