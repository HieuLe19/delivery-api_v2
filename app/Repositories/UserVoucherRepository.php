<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserVoucherRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserVoucherRepository extends RepositoryInterface
{
    //
}
