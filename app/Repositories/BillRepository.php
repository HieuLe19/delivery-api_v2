<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DeviceTokenRepository.
 *
 * @package namespace App\Repositories;
 */
interface BillRepository extends RepositoryInterface
{
    /**
     * Get list bill of user
     * @param integer $userId
     * @param array $billStatus
    */
    public function getListByUser($userId, $billStatus);
    
    /**
     * Get Unassign bill (user_id = NULL)
    */
    public function getUnAssignBill();
 
    public function getByOverDueFinishDate();

    public function countByDate($from, $to, $status = null);

}