<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DistrictRepositoryRepository.
 *
 * @package namespace App\Repositories;
 */
interface DistrictRepository extends RepositoryInterface
{
    //
}
