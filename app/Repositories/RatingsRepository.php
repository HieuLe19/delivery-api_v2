<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RatingsRepository.
 *
 * @package namespace App\Repositories;
 */
interface RatingsRepository extends RepositoryInterface
{
    //
}
