<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MasterSettingRepository;
use App\Models\MasterSetting;
use App\Validators\MasterSettingValidator;

/**
 * Class MasterSettingRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MasterSettingRepositoryEloquent extends BaseRepository implements MasterSettingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MasterSetting::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
