<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\NotificationRepository;
use App\Models\Notification;
use App\Validators\NotificationValidator;

/**
 * Class NotificationRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class NotificationRepositoryEloquent extends BaseRepository implements NotificationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }

    public function getList($status) 
    {
        return $this->model::select('*')
            ->where('status', '=', $status)
            ->get();
    }

    public function findById($id) 
    {
        return $this->model
            ::select(
                "id as Id", 
                'title as Title', 
                'description as Description',
                'target_link as URL',
                'status as Status',
                'user_type as To User')
            ->where('id', $id)
            ->first();
    }

    
}
