<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserNotificationRepository;
use App\Models\UserNotification;
use App\Validators\UserNotificationValidator;

/**
 * Class UserNotificationRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserNotificationRepositoryEloquent extends BaseRepository implements UserNotificationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserNotification::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return UserNotificationValidator::class;
    }

    public function getListByUser($userId)
    {
        return $this->model::select(['id', 'notification_id', 'read_at'])
            ->with('notification')
            ->where('user_id', $userId)
            ->paginate();
    }

    public function countTotalUnread($userId)
    {
        return $this->model
            ->where('user_id', $userId)
            ->whereNull('read_at')
            ->count();
    }
    
}
