<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ratingsRepository;
use App\Models\Ratings;
use App\Validators\RatingsValidator;

/**
 * Class RatingsRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class RatingsRepositoryEloquent extends BaseRepository implements RatingsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Ratings::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RatingsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
