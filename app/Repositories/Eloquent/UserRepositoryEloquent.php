<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRepository;
use App\Models\User;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function searchUser($conditions)
    {
        $result = $this->model::select('*');

        if (isset($conditions['is_online']) && 
            $conditions['is_online'] != USER_IS_ALL_STATUS)
        {
            $result->where('is_online', '=', $conditions['is_online']);
        }

        if (isset($conditions['status']) && $conditions['status'] != USER_ALL_STATUS) {
            $result->where('status', '=', $conditions['status']);
        }

        return $result->get();
    }

    public function byLocation($lat, $long, $distance)
    {
        $halls = $this->model
            ::select(array(
                'id',
                'status',
                'code',
                'full_name',
                'reward_point',
                'latitude as lat',
                'longitude as long'))
            ->whereRaw("`longitude` <> '' AND `latitude` <> ''")
            ->whereRaw('ACOS(SIN(RADIANS(`latitude`))*SIN(RADIANS('.$lat.'))'
                        .' + COS(RADIANS(`latitude`))*COS(RADIANS('.$lat.'))*COS(RADIANS(`longitude`)'
                        .' - RADIANS('.$long.')))*6380 <= '.$distance)
            ->where('is_online', USER_IS_ONLINE_STATUS)
            ->get();
        return $halls;
    }
    
    public function getListByDistrict($districtId)
    {
        return $this->model::select([
                    'id',
                    'area',
                    'status',
                    'code',
                    'full_name',
                    'reward_point',
                    'area_working',
                    'latitude as lat',
                    'longitude as long'
                ])
                ->where('area_working', $districtId)
                ->whereIn('status', [USER_ACTIVE_STATUS, USER_WARNING_STATUS])
                ->get();
    }
}
