<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserVoucherRepository;
use App\Models\UserVoucher;
use App\Validators\UserVoucherValidator;

/**
 * Class UserVoucherRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserVoucherRepositoryEloquent extends BaseRepository implements UserVoucherRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserVoucher::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
