<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserScheduleRepository;
use App\Models\UserSchedule;
use App\Validators\UserScheduleValidator;

/**
 * Class UserScheduleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserScheduleRepositoryEloquent extends BaseRepository implements UserScheduleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserSchedule::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
