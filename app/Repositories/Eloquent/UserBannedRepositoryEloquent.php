<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserBannedRepository;
use App\Models\UserBanned;
use App\Validators\UserBannedValidator;

/**
 * Class UserBannedRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserBannedRepositoryEloquent extends BaseRepository implements UserBannedRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserBanned::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return UserBannedValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
