<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrderRepository;
use App\Models\Order;
use App\Validators\OrderValidator;

/**
 * Class OrderRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OrderValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getOrderByUser($userId, $status)
    {
        $result = $this->model->select('orders.*')
            ->join('user_earnings as ue', 'ue.order_id', 'orders.id')
            ->where('ue.type', USER_EARNING_TURN_TYPE)
            ->where('user_id', $userId);

        if ($status != ORDER_ALL_STATUS) {
            $result->where('status', $status);
        } else {
            $result->whereIn('status', [2, 3]);     //done and cancel order status.
        } 

        return $result->paginate();
    }
    
}
