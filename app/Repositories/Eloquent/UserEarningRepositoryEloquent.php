<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserEarningRepository;
use App\Models\UserEarning;
use App\Validators\UserEarningValidator;

/**
 * Class UserEarningRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserEarningRepositoryEloquent extends BaseRepository implements UserEarningRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserEarning::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    public function sumOfMonth($userId)
    {
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t');
        
        return $this->model::select([
                \DB::raw('SUM(total) as total, SUM(receive) as receive'), 
            ])
            ->where('type', USER_EARNING_DATE_TYPE)
            ->where('date', '>', $first_day_this_month)
            ->where('date', '<', $last_day_this_month)
            ->where('user_id', $userId)
            ->first();
    }
}
