<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\DeviceTokenRepository;
use App\Models\DeviceToken;

/**
 * Class DeviceTokenRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class DeviceTokenRepositoryEloquent extends BaseRepository implements DeviceTokenRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DeviceToken::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
