<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BillRepository;
use App\Models\Bill;
use Carbon\Carbon;

/**
 * Class BillTokenRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BillRepositoryEloquent extends BaseRepository implements BillRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Bill::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListByUser($userId, $billStatus)
    {
        $result = $this->model::
            select([
                'id', 'distance', 'client_id', 'amount',
                'from_address', 'to_address', 'status'
            ])
            ->with('client:name,phone,email,id');

        if(in_array(BILL_DONE_STATUS, $billStatus)) {
            $result->with('rating:bill_id,rating')
                ->where('read_status', BILL_HISTORY_UN_READ_STATUS);
        }
        $result->whereIn('status', $billStatus)
                ->where('user_id', $userId);

        return $result->get();
    }

    public function getUnAssignBill()
    {
        return $this->model::select([
                'latitude_from', 'longitude_from', 'longitude_to', 'latitude_to',
                'id', 'status', 'office_hours', 'finish_date', 'amount', 'area'
            ])
            ->whereNull('user_id')
            ->orderBy('finish_date')
            ->get();
    }

    public function getByOverDueFinishDate()
    {
        $currentDate = Carbon::now()->format('Y-m-d');
        $result = $this->model::select(['id', 'user_id', 'status', 'finish_date'])
            ->whereNotNull('user_id')
            ->whereIn('status', [BILL_IN_QUERY_STATUS, BILL_IN_PROCESS_STATUS])
            ->where('finish_date', '<', $currentDate);

        return $result->get();
    }

    public function countByDate($from, $to, $status = null)
    {
        $result = $this->model::select(['id', 'finish_at', 'finish_date'])
                ->where('finish_at', '>', $from)
                ->where('finish_at', '<', $to);
        
        if($status != null) {
            $result->where('status', $status);
        }
        
        return $result->count();
    }
}
