<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DeviceTokenRepository.
 *
 * @package namespace App\Repositories;
 */
interface DeviceTokenRepository extends RepositoryInterface
{

}
