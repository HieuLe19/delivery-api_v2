<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserNotificationRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserNotificationRepository extends RepositoryInterface
{
    public function getListByUser($userId);
    public function countTotalUnread($userId);
}
