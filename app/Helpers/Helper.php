<?php

namespace App\Helpers;

class Helper
{
    /**
     * Push notification
     * @param array $data
     * @return string|mixed|unknown
     */
    private function doPushNotification($data = [])
    {
        $result = $this->sendRequest('POST', FIREBASE_CLOUD_MESSAGE_URL, json_encode($data), true);
        return $result;
    }
    /**
     * Get data from API
     * @param unknown $method
     * @param unknown $url
     * @param string $data
     * @return string|mixed|unknown
     */
    private function sendRequest($method, $url, $data = false, $pushFCM = false)
    {
        $curl = curl_init();

        if ($pushFCM) {
            $headers = array(
                'Authorization: key=' . env('FIREBASE_SERVER_KEY'),
                'Content-Type: application/json'
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 500);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        $result = curl_exec($curl);
        if (curl_errno($curl)) {
            $result = 'Curl error: ' . curl_error($curl);
        } else {
            if ($result) {
                $result = preg_replace('/,\s*([\]}])/m', '$1', $result);
                $json = json_decode($result, true);
                if ($json != null) {
                    $result = $json;
                }
            }
        }

        curl_close($curl);
        dd($result);
        return $result;
    }
    /**
     * Check device type and push notification
     * @param array $data
     * @param array $devices
     * @return string|mixed|unknown
     */
    public function pushNotification($data, $devices)
    {
        $androidTokens = [];
        $iOSTokens = [];
        $result = null;
        if ($devices) {
            if (count($devices)) {
                foreach ($devices as $device) {
                    if (!empty($device['device_token'])) {
                        //Android device
                        if ($device['type'] == ANDROID_DEVICE) {
                            $androidTokens[] = $device['device_token'];
                        }
                        //iOS device
                        else if ($device['type'] === IOS_DEVICE) {
                            $iOSTokens[] = $device['device_token'];
                        }
                    }
                }
            } else {
                if (!empty($devices['device_token'])) {
                    $devices['device_token'];
                    //Android device
                    if ($devices['type'] == ANDROID_DEVICE) {
                        $androidTokens[] = $devices['device_token'];
                    }
                    //iOS device
                    else if ($devices['type'] === IOS_DEVICE) {
                        $iOSTokens[] = $devices['device_token'];
                    }
                }
            }
        }
        if ($androidTokens) {
            // Set up push
            $push = [
                "notification" => [
                    "title"      => (!empty($data['title'])) ? $data['title'] : PUSH_DEFAULT_TITLE,
                    "body"       => $data['body'],
                ],
                "data" => [
                    "title"      => (!empty($data['title'])) ? $data['title'] : PUSH_DEFAULT_TITLE,
                    // "open_url"   => $data['open_url'],
                    "body"       => $data['body']
                ],
                "registration_ids" => $androidTokens
            ];
            // Push notification
            $result = $this->doPushNotification($push);
        }
        if ($iOSTokens) {
            // Set up push
            $push = [
                "data" => [
                    // "open_url"   => $data['open_url'],
                ],
                "notification" => [
                    "title"      => (!empty($data['title'])) ? $data['title'] : PUSH_DEFAULT_TITLE,
                    "body"    => $data['body'],
                    "vibrate" => 1,
                    "sound"   => "default",
                ],
                "registration_ids" => $iOSTokens
            ];
            // Push notification
            $result = $this->doPushNotification($push);
        }
        return $result;
    }
}
