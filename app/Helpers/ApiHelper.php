<?php
namespace App\Helpers;

use App\Helpers\HttpResponseCode;

class ApiHelper
{
    /**
   * API response data
   *
   * @param type $responseData
   * @param type $statusCode
   * @param type $message
   * @return response
   */
  function responseApi($responseData, $statusCode = 200) {
    return response ()->json ( $responseData, $statusCode );
  }

  /**
   * API response error
   *
   * @param type $errorCode
   * @param type $message
   * @return response
   */
  public static function responseError($code = 500, $message = null) {
    $result = [
      'code' => $code,
      'message' => $message ?? HttpResponseCode::getMessageForCode($code)
    ];
    return response()->json ($result);
  }

  /**
   * API response success
   *
   * @param type $message
   * @return response
   */
  public static function responseSuccess($data = null, $message = null, $code = 200) {
    $result = [
      'code' => $code,
      'message' => $message ?? HttpResponseCode::getMessageForCode($code),
    ];


    if (!is_null($data)) {
      $result['data'] = $data;
    }
    
    return response ()->json ($result, $code);
  }


}

