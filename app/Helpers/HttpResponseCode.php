<?php

namespace App\Helpers;

class HttpResponseCode
{
    const HTTP_OK                       = 200;
    const HTTP_INTERNAL_SERVER_ERROR    = 500;
    const HTTP_BAD_REQUEST              = 400;
    const HTTP_UNAUTHORIZED             = 401;
    const HTTP_PRECONDITION_FAILED      = 412;
    const HTTP_NOT_FOUND                = 404;

    private static $messages = array(
        200 => '200 OK',
        401 => 'Unauthorized',
        400 => 'Bad Request',
        404 => 'Api Not Found',
        500 => 'DB error',
        412 => 'Params Fail'
    );

    public static function getMessageForCode($code)
    {
        return self::$messages[$code];
    }
}
