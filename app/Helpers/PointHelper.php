<?php
namespace App\Helpers;

class PointHelper 
{
    /**
     * Class is a point with latitude and longitude in Google map
    */

    private $latitude;
    private $longitude;

    public function __construct(float $lat, float $long)
    {
        $this->latitude  = $lat;
        $this->longitude = $long;
    }
    
    private function rad($x) 
    {
        return $x * M_PI /180;
    }

    /**
     * get Latitude
     * @return string
    */
    public function getLatitude() 
    {
        return $this->latitude;
    }

    public function getLongitude() 
    {
        return $this->longitude;
    }

    /**
     * calculate distance 2 point using lat-long
     * @param PointHelper $point
     * @return float (KM) 
    */
    public function calculateDistance(PointHelper $point)
    {   
        $Radius = 6378137; // Earth’s mean radius in meter
        $dLat = $this->rad($point->getLatitude() - $this->latitude);
        $dLong = $this->rad($point->getLongitude() - $this->longitude);
        $a = sin($dLat / 2) * sin($dLat / 2) +
            cos($this->rad($this->latitude)) * cos($this->rad($point->getLatitude())) *
            sin($dLong / 2) * sin($dLong / 2);
        $cal = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $distance = $Radius * $cal;
        
        return $distance / 1000; // return the distance in Kilometer 
    }
    
}

?>