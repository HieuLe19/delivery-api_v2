<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\DeviceTokenRepository::class, \App\Repositories\Eloquent\DeviceTokenRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRatingRepository::class, \App\Repositories\Eloquent\UserRatingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NotificationRepository::class, \App\Repositories\Eloquent\NotificationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserNotificationRepository::class, \App\Repositories\Eloquent\UserNotificationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RatingsRepository::class, \App\Repositories\Eloquent\RatingsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrderRepository::class, \App\Repositories\Eloquent\OrderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\Eloquent\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserBannedRepository::class, \App\Repositories\Eloquent\UserBannedRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserEarningRepository::class, \App\Repositories\Eloquent\UserEarningRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MasterSettingRepository::class, \App\Repositories\Eloquent\MasterSettingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BillRepository::class, \App\Repositories\Eloquent\BillRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\VoucherRepository::class, \App\Repositories\Eloquent\VoucherRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserVoucherRepository::class, \App\Repositories\Eloquent\UserVoucherRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DistrictRepository::class, \App\Repositories\Eloquent\DistrictRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserScheduleRepository::class, \App\Repositories\Eloquent\UserScheduleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductTypeRepository::class, \App\Repositories\Eloquent\ProductTypeRepositoryEloquent::class);
        //:end-bindings:
    }
}
