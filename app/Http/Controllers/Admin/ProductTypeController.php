<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ApiHelper;
use App\Repositories\ProductTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductTypeController extends Controller
{
    protected $repository;

    public function __construct(ProductTypeRepository $repository)
    {
        $this->repository = $repository;        
    }

    public function index()
    {
        $result = $this->repository->all(['id as value', 'name as label']);

        return ApiHelper::responseSuccess($result);
    }
}
