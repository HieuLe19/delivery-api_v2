<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ApiHelper;

use Illuminate\Support\Facades\Storage;
use App\Http\Requests\NotificationCreateRequest;
use App\Http\Requests\NotificationUpdateRequest;
use App\Repositories\NotificationRepository;
use App\Repositories\UserNotificationRepository;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;

/**
 * Class NotificationsController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class NotificationsController extends Controller
{
    /**
     * @var NotificationRepository
     */
    protected $repository;

    /**
     * @var userRepository
     */
    protected $userRepository;

    /**
     * @var userNotificationRepository
     */
    protected $userNotificationRepository;


    /**
     * NotificationsController constructor.
     *
     * @param NotificationRepository $repository
     */
    public function __construct
    (
        NotificationRepository $repository,
        UserNotificationRepository $userNotificationRepository,
        UserRepository $userRepository
    )
    {
        $this->repository = $repository;
        $this->userNotificationRepository = $userNotificationRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = $this->repository->getList(NOTIFICATION_PUBLISH_STATUS);

        return ApiHelper::responseSuccess($notifications);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NotificationCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(NotificationCreateRequest $request)
    {
        try {


            $params = $request->all();
            $notification = $this->repository->create($params);
            $users = null;
            if ($params['user_type'] == 0) {
                //all user
                $users = $this->userRepository->all('id');
            } else {
                $users = $this->userRepository
                    ->searchUser($this->getSearchCondition($params['user_type']));
            }

            foreach($users as $user) {
                $userNotiData = [
                    'user_id' => $user->id,
                    'notification_id' => $notification->id
                ];
                $this->userNotificationRepository->create($userNotiData);
            }
        } catch (\Exception $e) {
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess(null, 'Create Notification Success');
    }

    private function getSearchCondition($userType) 
    {
        $result = [];

        switch ($userType) {
            case 1:         //  online user
                $result['is_online'] = 1;
                break;
            case 2:     // offline user
                $result['online_user'] = 2;
            case 3:
                $result['status'] = USER_DISABLE_STATUS;
                break;
            default:
                break;    
        }

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = $this->repository->find($id);
        return ApiHelper::responseSuccess($notification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification = $this->repository->find($id);

        return view('notifications.edit', compact('notification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  NotificationUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(NotificationUpdateRequest $request, $id)
    {
        try {
            $notification = $this->repository->update($request->all(), $id);
        } catch (\Exception $e) {
            \Log::error($e->getMessage()); 

            return ApiHelper::responseError("Update Notification Fail");          
        }

        return ApiHelper::responseSuccess(null, "Update Notification Success");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
        } catch (\Exception $ex) {
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess();
    }
}
