<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Helpers\HttpResponseCode;
use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\MasterSettingUpdateRequest;
use App\Repositories\MasterSettingRepository;

/**
 * Class MasterSettingsController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class MasterSettingsController extends Controller
{
    /**
     * @var MasterSettingRepository
     */
    protected $repository;

    /**
     * MasterSettingsController constructor.
     *
     * @param MasterSettingRepository $repository
     */
    public function __construct(MasterSettingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masterData = $this->repository->first();

        return ApiHelper::responseSuccess($masterData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MasterSettingUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(MasterSettingUpdateRequest $request)
    {
        try {
            $this->repository->first()->update($request->all());
        } catch (\Exception $e) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR, 
                $e->getMessage()
            );
        }

        return ApiHelper::responseSuccess(null, "Update Master Data Success");
    }
}