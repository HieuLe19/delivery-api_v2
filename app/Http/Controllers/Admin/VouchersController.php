<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\VoucherCreateRequest;
use App\Http\Requests\VoucherUpdateRequest;
use App\Repositories\VoucherRepository;
use App\Validators\VoucherValidator;

/**
 * Class VouchersController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class VouchersController extends Controller
{
    /**
     * @var VoucherRepository
     */
    protected $repository;

    /**
     * VouchersController constructor.
     *
     * @param VoucherRepository $repository
     */
    public function __construct(VoucherRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentDate = Carbon::now();
        $vouchers = $this->repository
            ->findWhere([['time_end','>', $currentDate]]);

        return ApiHelper::responseSuccess($vouchers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VoucherCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(VoucherCreateRequest $request)
    {
        try {
            $randomString = substr(str_shuffle(PASSWORD_RANDOM_CHARS), 
                0,
                RANDOM_LENGTH);
            $currentDate = Carbon::now()->toString();
            $params = $request->all();
            $params['code'] = $randomString + $code;
 
           $voucher = $this->repository->create($params);
        } catch (\Exception $e) {
            return ApiHelper::responseError();            
        }

        return ApiHelper::responseSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $voucher = $this->repository->find($id);
        } catch (\Exception $ex) {
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess($voucher);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  VoucherUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(VoucherUpdateRequest $request, $id)
    {
        try {

            $voucher = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Voucher updated.',
                'data'    => $voucher->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Voucher deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Voucher deleted.');
    }
}
