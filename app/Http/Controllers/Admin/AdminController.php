<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param LoginRequest $request
     * @return \App\Helpers\response
     * @author HieuLe
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        if (!$token = auth('admin')->attempt($credentials)) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_UNAUTHORIZED,
                HttpResponseCode::getMessageForCode(HttpResponseCode::HTTP_UNAUTHORIZED)
            );
        }

        $data = [
            'token' => $token,
            'user_data' => auth('admin')->user(),
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
        return ApiHelper::responseSuccess($data, 'Login success');
    }
}
