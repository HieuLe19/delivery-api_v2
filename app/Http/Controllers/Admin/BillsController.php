<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use App\Http\Controllers\Controller;
use App\Repositories\BillRepository;
use App\Repositories\UserRepository;
use App\Repositories\MasterSettingRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Helpers\PointHelper;

class BillsController extends Controller
{
    protected $repository;
    protected $userRepository;
    protected $masterSettingRes;

    public function __construct(
        BillRepository $repository, 
        UserRepository $userRepository,
        MasterSettingRepository $masterSettingRes
    ) 
    {
        $this->repository = $repository;
        $this->masterSettingRes = $masterSettingRes;
        $this->userRepository = $userRepository;
    }

    public function store(Request $request) 
    {
        $params = $request->all();
        $params['finish_date'] = Carbon::now()->addDays(5);

        //start calculate distance
        $from = new PointHelper(
                floatval($params['latitude_from']), 
                floatval($params['longitude_from'])
            );
        $to = new PointHelper(
                floatval($params['latitude_to']), 
                floatval($params['longitude_to'])
            );

        $params['distance'] = $from->calculateDistance($to);
        
        try {
            $this->repository->create($params);
        } catch(Exception $ex) {
            dd($ex);
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess();
    }

    public function getData()
    {
        $firstDateOfMonth = Carbon::now()->firstOfMonth()->format('Y-m-d');
        $lastDateOfMonth = Carbon::now()->lastOfMonth()->format('Y-m-d');
        $cancelBill = $this->repository
                                ->countByDate($firstDateOfMonth, $lastDateOfMonth, BILL_CANCEL_STATUS);
        $totalBill = $this->repository->countByDate($firstDateOfMonth, $lastDateOfMonth);
        
        $onlineUser = $this->userRepository->searchUser(['is_online' => USER_IS_ONLINE_STATUS]);
        $allUser = $this->userRepository->all();

        $result = [
            'cancel_bill' => $cancelBill,
            'total_bill' => $totalBill,
            'done_bill' => $totalBill - $cancelBill,
            'all_user' => count($allUser),
            'online_user' => count($onlineUser)
        ];

        return ApiHelper::responseSuccess($result);
    }

    public function getUnAssignBill() 
    {
        $result = $this->repository->findWhere(
                ['user_id'=> NULL],
                [
                    "to_address", "product_name", "finish_date", "from_address",
                    "distance", "amount", "comment", "id"
                ]
            );

        return ApiHelper::responseSuccess($result);
    } 

    public function list(Request $request) 
    {
        $params = $request->all();
        $result = [];

        if(isset($params['status'])) {
            $result = $this->repository->find(['status' => $params['status']])
                ->orderBy('created_at');
        } else {
            $result = $this->repository->all()->orderBy('created_at');
        } 

        return ApiHelper::responseSuccess($result);
    }
}