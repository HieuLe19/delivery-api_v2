<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper;
use App\Repositories\UserRepository;
use App\Helpers\HttpResponseCode;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function list(Request $request)
    {
        $conditions = $request->all();
        $conditions['is_online'] = USER_IS_ALL_STATUS;
        $conditions['status'] = USER_ALL_STATUS;
        $result = $this->userRepository->searchUser($conditions);

        return ApiHelper::responseSuccess($result);
    }


    public function store (UserCreateRequest $request) 
    {
        $params = $request->all();
        $params['code'] = 
            $this->getCodeByNameAndCard(
                $request->get('full_name'),
                $request->get('id_card')
            );
        $path = $request->file('avt_url')->store(`avatars`);
        $params['avt_url'] = $path;
        $randomString = substr(str_shuffle(PASSWORD_RANDOM_CHARS), 
                            0,
                            RANDOM_LENGTH);
        $params['password'] = Hash::make($randomString);
        try {
            //create record in DB.
            DB::beginTransaction();
            $this->userRepository->create($params);
            DB::commit();
            
            Mail::send('emails.register', $params, function ($message) use ($request) {
                $message->subject("Register | FASTER SHIP");
                $message->to($request->get('email'), $request->get('email'));
            });

        } catch(\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());

            return ApiHelper::responseError(
                HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR, 
                $ex->getMessage()
            );
        }

        return ApiHelper::responseSuccess();
    }

    public function show($id)
    {
        $result = $this->userRepository->find($id);

        return ApiHelper::responseSuccess($result);
    }

    // get code by First character of name and first 4 character of id Card.
    private function getCodeByNameAndCard($name, $idCard) 
    {
        dd($name);
        $words = explode(" ", $name);
        $acronym = "";
        foreach ($words as $w) {
            $acronym .= $w[0];
        }
        
        return $acronym .= '-' . substr(strval($idCard), 4);
    }

    public function bannedUser($userId) 
    {
        try{
            $this->userRepository->find($userId)
                ->update(['status' => USER_PENDING_STATUS]);
        } catch(\Exception $ex) {
            return ApiHelper::responseError();
        }
        
        return ApiHelper::responseSuccess();
    }

    public function update($id, UserUpdateRequest $request) 
    {
        try{
            $this->userRepository->update($request->all(), $id);
        } catch(\Exception $ex) {
            Log::error($ex->getMessage());
            return ApiHelper::responseError(500, "Somethings wrong");
        }

        return ApiHelper::responseSuccess(null,"Update user info success");
    }
}
