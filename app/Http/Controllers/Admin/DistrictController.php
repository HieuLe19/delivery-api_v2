<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ApiHelper;
use App\Repositories\DistrictRepository;

class DistrictController extends Controller
{
    protected $repository;

    public function __construct(DistrictRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList() 
    {
        $result = $this->repository->all(['id as value', 'name as label']);

        return ApiHelper::responseSuccess($result);
    }
}
