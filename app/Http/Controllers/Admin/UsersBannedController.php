<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Helpers\ApiHelper;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserBannedCreateRequest;
use App\Http\Requests\UserBannedUpdateRequest;
use Illuminate\Support\Facades\Log;
use App\Repositories\UserBannedRepository;
use App\Validators\UserBannedValidator;

/**
 * Class UserBannedsController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class UsersBannedController extends Controller
{
    /**
     * @var UserBannedRepository
     */
    protected $repository;

    /**
     * @var UserBannedValidator
     */
    protected $validator;

    /**
     * UsersBannedController constructor.
     *
     * @param UserBannedRepository $repository
     */
    public function __construct(UserBannedRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $userBanned = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userBanned,
            ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserBannedCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store($user_id, UserBannedCreateRequest $request)
    {
        try {
            $params = array_merge(['user_id' => $user_id], $request->all());
            if($params['type'] == USER_BAN_5_DAYS) {
                $params['from_date'] = Carbon::now();
                $params['to_date'] =  Carbon::now()->add(5, 'day');
            }
            $userBanned = $this->repository->create($params);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userBanned = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userBanned,
            ]);
        }

        return view('userBanneds.show', compact('userBanned'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userBanned = $this->repository->find($id);

        return view('userBanneds.edit', compact('userBanned'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserBannedUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserBannedUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userBanned = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'UserBanned updated.',
                'data'    => $userBanned->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'UserBanned deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'UserBanned deleted.');
    }
}
