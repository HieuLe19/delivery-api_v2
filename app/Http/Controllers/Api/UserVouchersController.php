<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserVoucherRepository;

class UserVouchersController extends Controller
{
    protected $repository;

    public function __construct(UserVoucherRepository $repository)
    {
        $this->repository = $repository;
    }

    public function list() 
    {
        $userId = auth()->user()->id;
        $result = $this->repository
            ->with('voucher')
            ->findWhere([
                'user_id' => $userId,
                'status' => VOUCHER_IN_QUERY_STATUS
            ]);

        return ApiHelper::responseSuccess($result);
    }
}
