<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\RatingsCreateRequest;
use App\Http\Requests\RatingsUpdateRequest;
use App\Repositories\RatingsRepository;
use App\Validators\RatingsValidator;

/**
 * Class RatingsController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class RatingsController extends Controller
{
    /**
     * @var RatingsRepository
     */
    protected $repository;

    /**
     * @var RatingsValidator
     */
    protected $validator;

    /**
     * RatingsController constructor.
     *
     * @param RatingsRepository $repository
     * @param RatingsValidator $validator
     */
    public function __construct(RatingsRepository $repository, RatingsValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $ratings = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $ratings,
            ]);
        }

        return view('ratings.index', compact('ratings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RatingsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(RatingsCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $rating = $this->repository->create($request->all());

            $response = [
                'message' => 'Ratings created.',
                'data'    => $rating->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rating = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $rating,
            ]);
        }

        return view('ratings.show', compact('rating'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rating = $this->repository->find($id);

        return view('ratings.edit', compact('rating'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RatingsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(RatingsUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $rating = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Ratings updated.',
                'data'    => $rating->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Ratings deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Ratings deleted.');
    }
}
