<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\NotificationRepository;

class NotificationController extends Controller
{
    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function detail($id) 
    {
        $data = null;
        try {
            $data = $this->repository->find($id, [
                                    'id', 
                                    'title', 
                                    'description', 
                                    'content', 
                                    'target_link',
                                    'type',
                                    'sub_id'
                                ]);
        } catch(\Exception $ex) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR, 
                $ex->getMessage());
        }

        return ApiHelper::responseSuccess($data);
    }
}
