<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserCreateRequest;
use App\Repositories\UserRepository;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    protected $userRepository;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param LoginRequest $request
     * @return \App\Helpers\response
     * @author HieuLe
     */
    public function login(LoginRequest $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];
        if (!$token = auth('api')->attempt($credentials)) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_UNAUTHORIZED,
                HttpResponseCode::getMessageForCode(HttpResponseCode::HTTP_UNAUTHORIZED)
            );
        }

        $data = [
            'token' => $token,
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ];
        return ApiHelper::responseSuccess($data, 'Login success');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        $oldPass = $request->get('password');
        if (auth()->attempt([
            'email' => $user->email,
            'password' => $oldPass
        ])) {
            if ($request->get('new_password') == $request->get('confirm_password')) {
                $user->password = \Hash::make($request->get('new_password'));
                $user->save();
                return ApiHelper::responseSuccess(null, 'Change password success');
            } else {
                return ApiHelper::responseError(HttpResponseCode::HTTP_PRECONDITION_FAILED, "2 password doesn't match");
            }
        }

        return ApiHelper::responseError(HttpResponseCode::HTTP_PRECONDITION_FAILED, 'Current password incorrect');
    }

    public function register(UserCreateRequest $request)
    {
        //dd($request->all());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $userData = $this->userRepository->find(auth()->user()->id);
        return ApiHelper::responseSuccess($userData);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return ApiHelper::responseSuccess(null, "Logout success");
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
