<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserRatingCreateRequest;
use App\Http\Requests\UserRatingUpdateRequest;
use App\Repositories\UserRatingRepository;
use App\Validators\UserRatingValidator;
use App\Http\Controllers\Controller;

/**
 * Class UserRatingsController.
 *
 * @package namespace App\Http\Controllers;
 */
class UserRatingsController extends Controller
{
    /**
     * @var UserRatingRepository
     */
    protected $repository;

    /**
     * @var UserRatingValidator
     */
    protected $validator;

    /**
     * UserRatingsController constructor.
     *
     * @param UserRatingRepository $repository
     * @param UserRatingValidator $validator
     */
    public function __construct(UserRatingRepository $repository, UserRatingValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $userRatings = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userRatings,
            ]);
        }

        return view('userRatings.index', compact('userRatings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRatingCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserRatingCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $userRating = $this->repository->create($request->all());

            $response = [
                'message' => 'UserRating created.',
                'data'    => $userRating->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userRating = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userRating,
            ]);
        }

        return view('userRatings.show', compact('userRating'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userRating = $this->repository->find($id);

        return view('userRatings.edit', compact('userRating'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserRatingUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserRatingUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userRating = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'UserRating updated.',
                'data'    => $userRating->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'UserRating deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'UserRating deleted.');
    }
}
