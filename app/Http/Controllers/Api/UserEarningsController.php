<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use App\Models\MasterSetting;
use App\Repositories\MasterSettingRepository;
use App\Repositories\UserEarningRepository;

/**
 * Class UserEarningsController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class UserEarningsController extends Controller
{
    /**
     * @var UserEarningRepository
     */
    protected $repository;

    /**
     * @var UserEarningRepository
     */
    protected $masterSettingRepository;
    /**
     * UserEarningsController constructor.
     *
     * @param UserEarningRepository $repository
     * @param MasterSettingRepository $masterSettingRepository 
     */
    public function __construct(
        UserEarningRepository $repository, 
        MasterSettingRepository $masterSettingRepository
    )
    {
        $this->repository = $repository;
        $this->masterSettingRepository = $masterSettingRepository;
    }

    /**
     * get User Earning by date 
     * @params $date must be y-m-d format 
    */
    public function getByDate(Request $request)
    {
        $result = $this->repository
            ->findWhere([
                    'user_id' => auth()->user()->id,
                    'date' => $request->get('date'),
                    'type' => USER_EARNING_DATE_TYPE
                ],
                ['id', 'total', 'receive', 'date'])         //get only this attribute
            ->first();
        
        //convert data
        $result->app_fee = ($result->total - $result->receive) *1000; //phi su dung ung dung
        $result->total *= 1000;
        $result->receive *= 1000;
        
        return ApiHelper::responseSuccess($result);
    }
    
    public function totalOfMonth()
    {
        $result = $this->repository->sumOfMonth(auth()->user()->id);
        
        //convert data
        $result->app_fee = ($result->total - $result->receive) *1000;
        $result->total *= 1000;
        $result->receive *= 1000;
        
        return ApiHelper::responseSuccess($result);
    }
}
