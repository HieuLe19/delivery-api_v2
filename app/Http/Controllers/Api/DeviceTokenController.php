<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeviceTokenRequest;
use App\Repositories\DeviceTokenRepository;
use Illuminate\Http\Request;

class DeviceTokenController extends Controller
{
   protected $deviceTokenRepository;

   public function __construct(DeviceTokenRepository $deviceTokenRepository)
   {
       $this->deviceTokenRepository = $deviceTokenRepository;
   }

    /**
     * create or update DeviceToken
     * @param DeviceTokenRequest $request
     * @return \App\Helpers\response
     * @author Hieule
     */
    public function updateDeviceToken(DeviceTokenRequest $request)
    {
        $user = auth('api')->user();
        $deviceToken = $this->deviceTokenRepository->findWhere(['user_id' => $user->id])->first();
        $data = [
            'device_token'=> $request->get('device_token'),
            'type' => $request->get('type'),
            'uuid' => $request->get('uuid'),
            'user_id' => $user->id,
        ];

        if($deviceToken){
            try{
                $this->deviceTokenRepository->update($data, $deviceToken->id);
            }catch (\Exception $exception){
                \Log::error('Update device token: ' . $exception->getMessage());
                return ApiHelper::responseError(500, 'Update device token fail.');
            }
        }else{
            try{
                $this->deviceTokenRepository->create($data);
            }catch (\Exception $exception){
                \Log::error('Create device token: ' . $exception->getMessage());
                return ApiHelper::responseError(500, 'Update device token fail.');
            }
        }

        return ApiHelper::responseSuccess();
    }
}
