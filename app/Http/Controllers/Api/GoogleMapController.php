<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use FarhanWazir\GoogleMaps\GMaps;

class GoogleMapController extends Controller
{
    public function direction() 
    {
        $config['center'] = 'Air Canada Centre, Toronto';
        $config['zoom'] = "16";
        $config['map_height'] = '500px';
        $config['geocodeCaching'] = true;

        $config['scrollwheel'] = false;

        $config['directions'] = true;
        $config['directionsStart'] = 'Air Canada Centre, Toronto';
        $config['directionsEnd'] = 'Yorkdale, Toronto';
        $config['directionsDivID'] = 'directionsDiv';

        $gmaps = new GMaps();
        $gmaps->initialize($config);
        // dd($gmaps);
        $map = $gmaps->create_map();

        dd($map);
        return view('google-map')->with('map', $map);
    }
}
