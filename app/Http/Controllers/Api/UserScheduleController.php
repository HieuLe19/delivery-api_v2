<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserScheduleRequest;
use App\Repositories\UserScheduleRepository;

class UserScheduleController extends Controller
{
    protected $repository;

    public function __construct(UserScheduleRepository $repository)
    {
        $this->repository = $repository;        
    }

    public function store(CreateUserScheduleRequest $request)
    {
        $params = $request->all();
        $params['user_id'] = auth()->user()->id;                                                                                            
        try {
            $data = $this->repository->findWhere([
                    'date' => $request->get('date'),
                    'user_id' => $params['user_id']
                ])
                ->first();

            if($data) {
                $this->repository->update($params, $data->id);
            } else {
                $this->repository->create($params);
            }
        } catch(\Exception $ex) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_BAD_REQUEST,
                $ex->getMessage()
            );
        }

        return ApiHelper::responseSuccess();
    }
}
