<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\VoucherRepository;
use App\Helpers\ApiHelper;

class VouchersController extends Controller
{
    protected $repository;

    public function __construct(VoucherRepository $repository)
    {
        $this->repository = $repository;
    }

    public function detail($id) 
    {
        try{
            $voucher = $this->repository->find($id);
        } catch (\Exception $ex) {
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess($voucher);
    }
}
