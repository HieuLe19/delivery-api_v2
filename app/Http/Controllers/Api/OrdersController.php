<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\OrderCreateRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Http\Requests\OrderGetListRequest;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Repositories\MasterSettingRepository;
use App\Repositories\BillRepository;
use App\Repositories\UserEarningRepository;
use App\Validators\OrderValidator;
use App\Http\Controllers\Controller;

/**
 * Class OrdersController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class OrdersController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $repository;
    protected $userRepository;
    protected $masterSettingRepository;
    protected $userEarningRepository;
    protected $billRepository;

    /**
     * @var OrderValidator
     */
    protected $validator;

    /**
     * OrdersController constructor.
     *
     * @param OrderRepository $repository
     */
    public function __construct(
        OrderRepository $repository,
        UserRepository $userRepository,
        MasterSettingRepository $masterSettingRepository,
        UserEarningRepository $userEarningRepository,
        BillRepository $billRepository
    )
    {
        $this->repository = $repository;
        $this->billRepository = $billRepository;
        $this->userRepository = $userRepository;
        $this->userEarningRepository = $userEarningRepository;
        $this->masterSettingRepository = $masterSettingRepository;
    }

    public function getOrderHistory(OrderGetListRequest $request)
    {
        $user = auth('api')->user();
        $result = $this->repository->getOrderByUser($user->id, 
            $request->get('status'));

        return ApiHelper::responseSuccess($result->all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  OrderCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(OrderCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $order = $this->repository->create($request->all());

            $response = [
                'message' => 'Order created.',
                'data'    => $order->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $order,
            ]);
        }

        return view('orders.show', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OrderUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateOrderStatus($id, OrderUpdateRequest $request)
    {
        try {
            $this->repository->update([
                'status' => $request->get('status')],
                $id);

            return ApiHelper::responseSuccess();

        } catch (\Exception $e) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR, 
                $e->getMessage());
        }
    }

    /**
     * 
    */
    public function pickUser(Request $request)
    {
        
    }

    /**
     * @param $id: orderId
        *- cập nhật trạng thái user.
        *- Lấy các thông số từ master_settings để tính tổng tiền.
        *- Tạo record mới ở table user_earnings
        *- Cộng giá tiền record này vào record tổng tiền của ngày 
        * của table user_earnings (type = 1) 
    */
    public function finish($id, Request $request) 
    {
        $userId = auth()->user()->id;
        try {
            $orderData = $this->repository->find($id);
        } catch (\Exception $ex) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_PRECONDITION_FAILED,
                "Order Id not valid"
            );
        }
        
        $amount = $this->calculateTotalEarning($orderData);
        $userEarningData = [
            'order_id'  => $id,
            'date'      => Carbon::now(),
            'total'     => $amount['total'],
            'user_id'   => $userId,
            'receive'   => $amount['receive']
        ];

        //create record for each bill (with default type = 0).
        $this->userEarningRepository->create($userEarningData);

        $totalMoneyDate = $this->userEarningRepository
            ->findWhere([
                'date' => Carbon::now()->format('Y-m-d'),
                'type' => 1,
            ])
            ->first();
        if($totalMoneyDate) {
            $newTotalAmount   = $totalMoneyDate->total + $amount['total'];
            $newReceiveAmount = $totalMoneyDate->receive + $amount['receive'];
            $totalMoneyDate->update(['total' => $newTotalAmount, 'receive' => $newReceiveAmount]);
        } else {
            $userEarningData['type'] = 1;
            $this->userEarningRepository->create($userEarningData);
        }

        $this->userRepository
            ->update(['is_online' => USER_IS_ONLINE_STATUS], $userId);

        return ApiHelper::responseSuccess();
    }

    /*
    Calculate total money of 1 order
    */
    private function calculateTotalEarning($orderData)
    {
        $billData = $this->billRepository->find(intval($orderData->bill_id));        
        $masterData = $this->masterSettingRepository->first();
        $shippingFee = $masterData->unit_price * $billData->distance;

        $result = [
            'total' => $shippingFee + $orderData->amount,
            'receive' => $masterData->trade * $shippingFee
        ];

        return $result;
    }
}
