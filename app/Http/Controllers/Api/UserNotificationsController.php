<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserNotificationCreateRequest;
use App\Http\Requests\UserNotificationUpdateRequest;
use App\Repositories\UserNotificationRepository;
use App\Validators\UserNotificationValidator;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserNotificationsController.
 *
 * @package namespace App\Http\Controllers\Api;
 */
class UserNotificationsController extends Controller
{
    /**
     * @var UserNotificationRepository
     */
    protected $repository;

    /**
     * @var UserNotificationValidator
     */
    protected $validator;

    /**
     * UserNotificationsController constructor.
     *
     * @param UserNotificationRepository $repository
     * @param UserNotificationValidator $validator
     */
    public function __construct(UserNotificationRepository $repository, UserNotificationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $userId = Auth::guard('api')->user()->id;
        $userNotifications = $this->repository->getListByUser($userId);

        $paginate = [
            'total' => $userNotifications->total(),
            'per_page' => $userNotifications->perPage(),
            'current_page' => $userNotifications->currentPage(),
            'last_page' => $userNotifications->lastPage(),
            'from' => $userNotifications->firstItem(),
            'to' => $userNotifications->lastItem(),
        ];

      return ApiHelper::responseSuccess($userNotifications->all());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserNotificationCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserNotificationCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $userNotification = $this->repository->create($request->all());

            $response = [
                'message' => 'UserNotification created.',
                'data'    => $userNotification->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserNotificationUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserNotificationUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userNotification = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'UserNotification updated.',
                'data'    => $userNotification->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'UserNotification deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'UserNotification deleted.');
    }

    /**
     * Count total unread Notification by user
     * @author HieuLe
     * return responseJson
    */
    public function countUnread()
    {   
        $userId = Auth::guard('api')->user()->id;

        $result['total_unread'] = $this->repository->countTotalUnread($userId);

        return ApiHelper::responseSuccess($result);
    }

    /**
     * Read Notification
     * 
    */
    public function read($notification_id)
    {
        $user_id = Auth::guard('api')->user()->id;
        
        try {
            $user_notification = $this->repository->findWhere([
                'notification_id' => $notification_id,
                'user_id' => $user_id])
                ->first();
            if ($user_notification) {
                $this->repository->update(
                    ['read_at' => Carbon::now()], 
                    $user_notification->id);
            } else {
                return ApiHelper:: responseError(
                    HttpResponseCode::HTTP_PRECONDITION_FAILED, 
                    'Notification not found');
            }
            
        } catch(\Exception $e) {
            return ApiHelper::responseError();
        };
      
        return ApiHelper::responseSuccess();
    }
}
