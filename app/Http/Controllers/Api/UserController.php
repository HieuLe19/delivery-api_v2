<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Mail;
use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\UpdateUserOnlineStatus;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Update Latitude and Longitude user 
     */
    public function updatePosition(Request $request)
    {
        $userId = auth('api')->user()->id;
        $params = $request->all();
        unset($params['_method']);
        $this->userRepository->update($params, $userId);

        return ApiHelper::responseSuccess();
    }

    public function updateOnlineStatus(UpdateUserOnlineStatus $request)
    {
        $userId = auth('api')->user()->id;

        try {
            $this->userRepository->update(['is_online' => $request->get('is_online')], $userId);
        } catch (\Exception $exception) {
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess();
    }

    public function update(Request $request) 
    {
        $userId = auth()->user()->id;
        $params = $request->all();
        // $avatar = $request->image;
        // $path = $request->file('avatar')->store('avatars');
        // $params['avt_url'] = $path;
        try {
            $this->userRepository->update($params, $userId);
        } catch(\Exception $e) {
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess();
    }

    public function getListByLocation() 
    {
        $lat = "10.878365740202954";
        $long = "106.80720221846494";
        $distance = 3;
        $result = $this->userRepository->byLocation($lat, $long, $distance);

        return ApiHelper::responseSuccess($result);
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $user = $this->userRepository
            ->findWhere(['email' => $request->get('email')])
            ->first();

        if($user == null) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_PRECONDITION_FAILED, 
                'email not found!'
            );
        }
        $randomString = substr(str_shuffle(PASSWORD_RANDOM_CHARS), 0, RANDOM_LENGTH);
        $data = [
            'full_name' => $user->full_name,
            'password' => $randomString
        ];
        try {
            Mail::send('emails.forgot-password', $data, function ($message) use ($request) {
                $message->subject("Forgot password | FASTER SHIP");
                $message->to($request->get('email'), $request->get('email'));
            });

            //update DB.
            $this->userRepository->update(
                ['password' => \Hash::make($randomString)], 
                $user->id
            );
        } catch(\Exception $ex) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR,
                $ex->getMessage()
            );
        }
        
        return ApiHelper::responseSuccess();
    }
}
