<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use App\Repositories\DistrictRepository;

class DistrictController extends Controller
{
    protected $repository;

    public function __construct(DistrictRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList() 
    {
        $result = $this->repository->all(['id as value', 'name as label']);

        return ApiHelper::responseSuccess($result);
    }
}
