<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderGetListRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Models\Bill;
use App\Repositories\BillRepository;
use App\Repositories\MasterSettingRepository;
use App\Repositories\UserEarningRepository;
use App\Repositories\UserRepository;
use Exception;

class BillsController extends Controller
{
    protected $billRepository;
    protected $masterSettingRepository;
    protected $userEarningRepository;
    protected $userRepository;

    public function __construct(
        BillRepository $billRepository,
        UserRepository $userRepository, 
        MasterSettingRepository $masterSettingRepository,
        UserEarningRepository $userEarningRepository
    )
    {
        $this->billRepository = $billRepository;
        $this->userRepository = $userRepository;        
        $this->masterSettingRepository = $masterSettingRepository;
        $this->userEarningRepository = $userEarningRepository;
    }

    public function getHistory(OrderGetListRequest $request) 
    {
        $doneStatus = [BILL_DONE_STATUS, BILL_CANCEL_STATUS];
        $result = $this->billRepository
            ->getListByUser(auth()->user()->id, $doneStatus);

        return ApiHelper::responseSuccess($result);
    }

    public function detail($id) 
    {
        $result = $this->billRepository->with('client:id,name,email,phone')
            ->find($id);

        return ApiHelper::responseSuccess($result);
    }

    public function list() 
    {
        $inProcessStatus = [BILL_IN_PROCESS_STATUS, BILL_IN_QUERY_STATUS];
        $result = $this->billRepository
            ->getListByUser(auth()->user()->id, $inProcessStatus);

        return ApiHelper::responseSuccess($result);
    }

    public function updateBillStatus($id, OrderUpdateRequest $request) 
    {
        try {
            $this->billRepository->update(
                ['status' => $request->get('status')],
                $id
            );
        } catch(\Exception $ex) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR, 
                $ex->getMessage());
        }

        return ApiHelper::responseSuccess();
    }


    /**
     * @param $id: orderId
        *- cập nhật trạng thái user.
        *- Lấy các thông số từ master_settings để tính tổng tiền.
        *- Tạo record mới ở table user_earnings
        *- Cộng giá tiền record này vào record tổng tiền của ngày 
        * của table user_earnings (type = 1) 
    */
    public function finish($id, Request $request) 
    {
        $userId = auth()->user()->id;
        $isCancel = $request->get('is_cancel'); //boolean
        try {
            $billData = $this->billRepository->find($id);
        } catch (\Exception $ex) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_PRECONDITION_FAILED,
                "Bill Not Found"
            );
        }
        $amount = $this->calculateTotalEarning($billData, $isCancel);
        $userEarningData = [
            'order_id'  => $id,
            'date'      => Carbon::now(),
            'total'     => $amount['total'],
            'user_id'   => $userId,
            'receive'   => $amount['receive']
        ];

        //create record for each bill (with default type = 0).
        $this->userEarningRepository->create($userEarningData);
        $totalMoneyDate = $this->userEarningRepository
            ->findWhere([
                'date' => Carbon::now()->format('Y-m-d'),
                'type' => 1,
            ])
            ->first();
        if($totalMoneyDate) {
            $newTotalAmount   = $totalMoneyDate->total + $amount['total'];
            $newReceiveAmount = $totalMoneyDate->receive + $amount['receive'];
            $totalMoneyDate->update([
                'total' => $newTotalAmount,
                'receive' => $newReceiveAmount
            ]);
        } else {
            $userEarningData['type'] = 1;
            $this->userEarningRepository->create($userEarningData);
        }

        $this->updateRewardPointUser($userId, $billData->distance);

        //update bill status
        $billUpdateData = [];
        if($isCancel) {
            $billUpdateData['status'] = BILL_CANCEL_STATUS;
            $path = $request->file('image')->store(`bills`);
            $billUpdateData['cancel_image'] = $path;
        } else {
            $billUpdateData['status'] = BILL_DONE_STATUS;            
        }
        $this->billRepository->update($billUpdateData, $id);

        return ApiHelper::responseSuccess();
    }


    private function updateRewardPointUser($userId, $bonus) 
    {
        $userData = $this->userRepository->find($userId);
        $userData->reward_point += $bonus;

        $userData->save();
    }


    /**
     * Calculate total money of 1 order
     * @param BillsModel $billData
     * @param Boolean $isCancel
    */
    private function calculateTotalEarning($billData, $isCancel)
    {
        $masterData = $this->masterSettingRepository->first();
        $shippingFee = $masterData->unit_price * $billData->distance;

        //percent fee subtract by cancel bill.
        $cancelFee = $isCancel ? (1 - $masterData->support_cancel_bill) : 1;

        $result = [
            'total' => $shippingFee,
            'receive' => $shippingFee * (1 - $masterData->trade) * $cancelFee
        ];

        return $result;
    }

    public function store(Request $request) 
    {

    }

    public function unAssignBill($id) 
    {
        $bill = null;
        try {
            $bill = $this->billRepository->findWhere([
                'id' => $id, 
                ['finish_date', '>', Carbon::now()->addDays(1)]
            ]);
        } catch (\Exception $ex) {
            return ApiHelper::responseError(
                HttpResponseCode::HTTP_PRECONDITION_FAILED,
                "Bill Not Found"
            );
        }

        if(count($bill)) {
            $this->billRepository->update(['user_id' => NULL], $id);
        } else {
            return ApiHelper::responseError(413, "Bill due date, you must complete");
        }
        
        return ApiHelper::responseSuccess();
    }

    /**
     * update read_status history
     * @param integer $id
     * @since 2020-12-27
     * @return JonResponse
    */
    public function updateReadHistory($id) 
    {
        try {
            $this->billRepository->update([
                'read_status' => BILL_HISTORY_READ_STATUS], 
                $id
            );
        } catch(Exception $ex) {
            return ApiHelper::responseError();
        }

        return ApiHelper::responseSuccess();
    }

}
