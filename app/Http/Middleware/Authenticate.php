<?php

namespace App\Http\Middleware;

use App\Helpers\ApiHelper;
use App\Helpers\HttpResponseCode;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }

    public function handle($request, Closure $next, ...$guards)
    {
        // check header accept application/json
        if($request->expectsJson() == false){
            return ApiHelper::responseError(HttpResponseCode::HTTP_BAD_REQUEST);
        }

        // check token
        if($this->authenticate($request, $guards) === false){
            return ApiHelper::responseError(HttpResponseCode::HTTP_UNAUTHORIZED,
                HttpResponseCode::getMessageForCode(HttpResponseCode::HTTP_UNAUTHORIZED));
        }

        return $next($request);
    }

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param $request
     * @param array $guards
     * @return boolean
     *
     */
    protected function authenticate($request, array $guards)
    {
        if (empty($guards)) {
            $guards = [null];
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        return false;
    }
}
