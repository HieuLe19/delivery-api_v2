<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
class NotificationCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
            'type' => 'required|in:0,1',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'title is required',
            'description.required' => 'description is required',
            'content.required' => 'content is required',
        ];
    }
}
