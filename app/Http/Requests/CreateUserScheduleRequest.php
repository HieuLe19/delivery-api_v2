<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;

class CreateUserScheduleRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'office_time' => 'integer|min:0|max:8',
            'out_office_time' => 'integer|min:0|max:5'
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Date is required'
        ];
    }
}
