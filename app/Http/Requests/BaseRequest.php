<?php

namespace App\Http\Requests;

use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

use App\Helpers\HttpResponseCode;
use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    protected $customCode = null;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }

    /**
     * @author HieuLe
     * @since 2020-10-31
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->addExtension('is_json', function ($attribute, $value, $parameters) {
            $result = json_decode($value, true);
            return $result && $value != $result;
        });
        $validator->addReplacer('is_json', function ($message, $attribute, $rule, $parameters, $validator) {
            return __("The :attribute must be JSON Object or JSON Array.", compact('attribute'));
        });
        $validator->after(function ($validator) {
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
        return $validator;
    }

    /**
     * @author HieuLe
     * @since 2020-10-31
     */
    protected function after($validator)
    {
    }

    /**
     * @author HieuLe
     * @since 2020-10-31
     */
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, response()->json([
            'code' => $this->customCode ? $this->customCode : HttpResponseCode::HTTP_PRECONDITION_FAILED,
            'message' => $validator->errors()->first()
        ], HttpResponseCode::HTTP_OK));
    }
}
