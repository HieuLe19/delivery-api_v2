<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;

class DeviceTokenRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_token' => 'required',
            'type' => 'required|integer|min:0|max:1',
            'uuid' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'uuid.required' => 'Uuid is required.',
            'device_token.required' => 'Device token is required.',
            'type.required' => 'type is required.',
            'type.integer' => 'type is must be an integer.',
            'type.min' => 'type must be 1:iOS or 0:Android.',
            'type.max' => 'type must be 1:iOS or 0:Android.',
            //'apns_token.required_if' => 'apns_token required with IOS device type!'
        ];
    }
}
