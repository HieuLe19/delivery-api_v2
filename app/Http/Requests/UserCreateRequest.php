<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\BaseRequest;

class UserCreateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'full_name' => 'required',
          'id_card' => 'required|Unique:users,id_card',
          'email' => 'required|email|Unique:users,email',
          'phone_number' => 'required|Unique:users,phone_number',
          'bank_account_number' => 'required',
          'bank_account_name' => 'required',
        ];
    }

    public function messages()
    {
      return [
        'email.required' => 'Email is required.',
        'email.email' => 'Email invalid.',
        'id_card.required' => 'ID Card is required',
        'phone_number.required' => 'Phone Number is required',
        'bank_account_number.required' => 'Bank account number is required',
        'bank_account_name.required' => 'Bank account name is required',
        'full_name.required' => 'Full name is required'
      ];
    }
}
