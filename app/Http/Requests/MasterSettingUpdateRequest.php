<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MasterSettingUpdateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'trade' => 'required|max:1|digits:1',
            // 'support_cancel_bill' => 'required|max:1',
            // 'unit_price' => 'required',
            // 'warning_point' => 'required',
            // 'max_bill_per_hour' => 'required'
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
