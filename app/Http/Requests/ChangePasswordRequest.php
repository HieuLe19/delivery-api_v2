<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;

class ChangePasswordRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'          => 'required|string',
            'new_password'      => 'required|string',
            'confirm_password'  => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Old password is required.',
            'new_password.required' => 'New password is required.',
            'confirm_password.required' => 'Confirm password is required.'
        ];
    }
}
