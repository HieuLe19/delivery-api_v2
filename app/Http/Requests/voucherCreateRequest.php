<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;


class voucherCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'discount' => 'required',
            'time_start' => 'required', 
            'time_end' => 'required'
        ];
    }

    public function message() 
    {
        return [
            'name.required' => 'Name must be required',
            'description.required' => 'Description must be required',
            'discount.required' => 'Discount must be required',
            'time_start.required' => 'Time start must be required',
            'time_end.required' => 'Time end must be required',
        ];
    }
}
