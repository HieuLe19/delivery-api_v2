<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;

class UserBannedCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:0,1',
        ];
    }

    public function message() 
    {
        return [
            'type.required' => 'banned type is required',
            'type.in' => 'type must be 0 or 1'
        ];
    }
}
