<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserEarning.
 *
 * @package namespace App\Models;
 */
class UserEarning extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'order_id',
        'date',
        'type',
        'total',
        'receive',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $appends = [
        'key',
    ];
    
    // public function getTotalAttribute() 
    // {
    //     return $this->total * 1000;
    // }

    public function getKeyAttribute() 
    {
        return strval($this->id);
    }

}
