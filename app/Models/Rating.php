<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Ratings.
 *
 * @package namespace App\Models;
 */
class Rating extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'user_id',
        'rating',
        'comment'
    ];

    protected $hidden = [
        'bill_id',

        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
