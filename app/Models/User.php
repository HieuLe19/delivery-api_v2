<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Driver.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'full_name',
        'email',
        'phone_number',
        'password',
        'latitude',
        'longitude',
        'id_card',
        'address',
        'phone_verify_at',
        'is_online',
        'reward_point',
        'status',
        'area_working',
        'bank_account_number',
        'bank_account_name',
        'avt_url'
    ];

    protected $appends = [
        'key',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'password'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function userBanned() 
    {
        return $this->hasMany(UserBanned::class);
    }

    public function getKeyAttribute() 
    {
        return strval($this->id);
    } 
}
