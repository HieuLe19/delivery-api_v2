<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;


class Bill extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'product_name',
        'client_id',
        'finish_at',
        'finish_date',
        'from_address',
        'to_address',
        'distance',
        'status',
        'read_status',
        'product_type_id',
        "amount",
        "comment",
        'user_id',
        'client_id',

        'longitude_from',
        'latitude_from',
        'latitude_to',
        "longitude_to",
        'cancel_image',
        
    ];

    protected $appends = [
        'key',
    ];

    protected $hidden = [
        
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function user() 
    {
        return $this->hasOne(User::class);
    }

    public function getKeyAttribute() 
    {
        return strval($this->id);
    } 

    public function client() 
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function rating() 
    {
        return $this->hasOne(Rating::class);
    }
}
