<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Notification.
 *
 * @package namespace App\Models;
 */
class Notification extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'description',
        'content',
        'target_link',
        'will_publish_at',
        'publish_at',
        'user_type',
        'status',
        'type',
        'sub_id'
        
    ];

    protected $appends = [
        'key'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getKeyAttribute() 
    {
        return strval($this->id);
    }
}
