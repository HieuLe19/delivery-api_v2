<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserBanned.
 *
 * @package namespace App\Models;
 */
class UserBanned extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
        'from_date',
        'to_date',
        'comment',
    ];

    protected $table = 'user_banned';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function user() 
    {
        return $this->belongsTo(User::class);
    }

}
