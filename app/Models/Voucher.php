<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

/**
 * Class Voucher.
 *
 * @package namespace App\Models;
 */
class Voucher extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'discount',
        'status',
        'time_start',
        'time_end',
    ];

    protected $appends = [
        'key', 'days_remain'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $table ="voucher";

    public function getDaysRemainAttribute() 
    {
        
        $currentDate = strtotime(Carbon::now());
        $timeEnd = strtotime(Carbon::create($this->time_end));
        $secs = $timeEnd - $currentDate;
        return intval($secs / 86400);
    }

    public function getKeyAttribute() 
    {
        return strval($this->id);
    } 
}
