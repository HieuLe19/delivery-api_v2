<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

class District extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['id', 'name'];

    protected $appends = [
        'key',
    ];

    public function getKeyAttribute() 
    {
        return strval($this->value);
    } 

    public $timestamps = true;
}
