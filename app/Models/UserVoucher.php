<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserVoucher.
 *
 * @package namespace App\Entities;
 */
class UserVoucher extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'voucher_id',
        'status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $appends = [
        'key'
    ];

    public function getKeyAttribute() 
    {
        return strval($this->id);
    }
    
    public function voucher() 
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }
}
