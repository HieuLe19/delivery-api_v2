<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserNotification.
 *
 * @package namespace App\Models;
 */
class UserNotification extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'notification_id',
        'read_at',
        'created_at',
    ];

    protected $appends = [
        'key'
    ];

    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    protected $table = 'user_notifications';

    public function notification()
    {
        return $this->belongsTo(Notification::class, 'id');
    }

    public function getCreatedAtAttribute($val) 
    {
        if (!$val) {
            return null;
        }
        if ($val < 1) {
            return Carbon::parse('1970-01-01 09:00:00')->format('d-m-Y-h:m:s');
        }
        return Carbon::parse($val)->format('d-m-Y-h:m:s');
    }

    public function getKeyAttribute() 
    {
        return strval($this->id);
    } 

    // public function getReadAtAttribute($val) 
    // {
    //     if (!$val) {
    //         return null;
    //     }

    //     return Carbon::parse($val)->format('d-m-Y-h:m:s');
    // }
}
