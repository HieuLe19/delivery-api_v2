<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
    Route::group(['prefix' => 'auth'], function ($router) {
        Route::post('login', 'Auth\LoginController@login');
        Route::post('forgot-password', 'UserController@forgotPassword');
    });
    Route::group([
        'middleware' => 'auth:api'
    ], function ($router) {
        Route::group(['prefix' => 'auth'], function ($router) {
            Route::post('/logout', 'Auth\LoginController@logout');
            Route::post('/register', 'Auth\LoginController@register');
            Route::post('/me', 'Auth\LoginController@me');
            Route::post('/change-password', 'Auth\LoginController@changePassword');
        });

        Route::group(['prefix' => 'user'], function ($router) {
            Route::put('/lat-long', 'UserController@updatePosition');
            Route::put('/online-status', 'UserController@updateOnlineStatus');
            Route::put('/', 'UserController@update');
            Route::get('/list-user', 'UserController@getListByLocation');
            Route::group(['prefix' => 'schedule'], function ($router) {
               Route::post('', 'UserScheduleController@store'); 
            });
        });

        Route::post('/update-token', 'DeviceTokenController@updateDeviceToken')->name('update_token');

        Route::group(['prefix' => 'notifications'], function ($router) {
            Route::get('/', 'UserNotificationsController@index');
            Route::get('/count', 'UserNotificationsController@countUnread');
            Route::post('read/{notification_id}', 'UserNotificationsController@read');
            Route::get('/{id}', 'NotificationController@detail');
        });

        Route::group(['prefix' => 'bills'], function ($router) {
            Route::get('/query', 'BillsController@list');
            Route::put('/{id}', 'BillsController@updateBillStatus');
            Route::get('/{id}', 'BillsController@detail');
            Route::get('/', 'BillsController@getHistory');
            Route::put('/un-assign/{id}', 'BillsController@unAssignBill');
            Route::post('/finished/{id}', 'BillsController@finish');
            Route::put('/read-status/{id}', 'BillsController@updateReadHistory');
        });
        Route::group(['prefix' => 'earnings'], function ($router) {
            Route::get('/', 'UserEarningsController@getByDate');
            Route::get('/month', 'UserEarningsController@totalOfMonth');
        });
        Route::group(['prefix' => 'vouchers'], function ($router) {
            Route::get('/', 'UserVouchersController@list');
            Route::get('/{id}', 'VouchersController@detail');
        });
        Route::group(['prefix' => 'district'], function ($router) {
            Route::get('/', 'DistrictController@getList');
        });
    });

    Route::group(['prefix' => 'google-map'], function ($router) {
        Route::get('/', 'GoogleMapController@direction');
    });
});
