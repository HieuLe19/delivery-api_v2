<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::post('login', 'AdminController@login');

    Route::group([
        //'middleware' => 'auth:admin'
    ], function ($router) {
        Route::group(['prefix' => 'notifications'], function ($router) {
            Route::post('/', 'NotificationsController@store');
            Route::get('/', 'NotificationsController@index');
            Route::get('/{id}', 'NotificationsController@show');
            Route::put('/{id}', 'NotificationsController@update');
            Route::delete('/{id}', 'NotificationsController@destroy');
        });

        Route::group(['prefix' => 'users'], function ($router) {
            Route::post('/', 'UserController@store');
            Route::get('/', 'UserController@list');
            Route::get('/{id}', 'UserController@show');
            Route::put('/{id}', 'UserController@update');
            Route::group(['prefix' => '/banned'], function ($router) {
                Route::post('/{user_id}', 'UsersBannedController@store');
            });
        });
        Route::group(['prefix' => 'vouchers'], function ($router) {
            Route::get('/', 'VouchersController@index');
            Route::get('/{id}', 'VouchersController@show');
            Route::post('/', 'VouchersController@store');
            Route::put('/{id}', 'VouchersController@update');
            Route::delete('/{id}', 'VouchersController@delete');
        });
        Route::group(['prefix' => 'bills'], function ($router) {
            Route::get('/', 'BillsController@getData');
            Route::post('/', 'BillsController@store');
            Route::get('/list', 'BillsController@list');
            Route::get('/list/no-user', 'BillsController@getUnAssignBill');
        });
        Route::group(['prefix' => 'settings'], function ($router) {
            Route::get('/', 'MasterSettingsController@index');
            Route::put('/', 'MasterSettingsController@update');
        });
        Route::group(['prefix' => 'product-type'], function ($router) {
            Route::get('/', 'ProductTypeController@index');
        });
        Route::group(['prefix' => 'district'], function ($router) {
            Route::get('/', 'DistrictController@getList');
        });
    });
});
