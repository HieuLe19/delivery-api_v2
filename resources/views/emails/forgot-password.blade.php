<div>
    <div>Chào {{ $full_name }}</div>
    <br>
    <div>Bạn nhận được email này bởi vì chúng tôi nhận được yêu cần reset mật khẩu từ bạn.</div>
    <div>Hãy sử dụng mật khẩu dưới đây làm mật khẩu mới cho tài khoản của bạn.</div>
    <br>
    <div style="color: #FFCF44!important;text-decoration:none!important">{{ $password }}</div>
    <br>
    <div>Cảm ơn bạn đã đồng hành,</div>
    <div>Đội ngũ Faster Ship.</div>
</div> 