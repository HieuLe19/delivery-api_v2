<div>
    <div>Chào {{$full_name}},</div>
    <br>
    <div>Chào mừng bạn tham gia vào đội ngũ FasterShip</div>
    <div>Bạn có thể đăng nhập vào ứng dụng với thông tin tài khoản dưới đây:</div>
    <div>Login: <label style="font-weight: bold;"> {{ $email }} </label></div>
    <div>Password: <label style="font-weight: bold;">{{  $password }}</label></div>
    <br>
    <div>Sau khi đăng nhập, bạn có thể thay đổi mật khẩu để dễ ghi nhớ hơn.</div>
    <br>
    <div>Để biết thêm chi tiết, bạn có thể liên lạc chúng tôi qua mail:<label style="color: #FFCF44!important;text-decoration:none!important">support_fastership@gmail.com</label></div>
    <br>
    <div>Chân thành,</div>
    <div>Đội ngũ FasterShip.</div>
</div>