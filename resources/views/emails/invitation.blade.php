<div>
    <div>Chào {{ $user->full_name }},</div>
    <br>
    <div>Chúng tôi rất vui khi được hợp tác với bạn.</div>
    <br>
    <div>Bạn có thể tải ứng dụng theo đường dẫn bên dưới:</div>
    <div>[iOS]: <label style="color: #FFCF44!important;text-decoration:none!important">https://itunes.apple.com/us/app/id1492986136?mt=8</label></-div>
    <div>[Android]: <label style="color: #FFCF44!important;text-decoration:none!important">http://play.google.com/store/apps/details?id=com.thirdkind.spotlight</label></div>
    <br>
    <div>Sau khi tải ứng dụng, Bạn có thể sử dụng ứng dụng với tài khoản dưới đây:</div>
    <div>Login: <label style="font-weight: bold;">{{ $email  }}</label></div>
    @if(isset($password))
        Password: <label style="font-weight: bold;">{{ $password }}</label>
    @endif
    <br>
    <div>Để biết thêm chi tiết, bạn có thể liên lạc chúng tôi qua mail:<label style="color: #FFCF44!important;text-decoration:none!important">support_fastership@gmail.com</label></div>
    <br>
    <div>Thân ái,</div>
    <div>Đội ngũ Faster Ship.</div>
</div>