<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">    
        {!! $map['js'] !!}
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCF4wYcheDcZj93bvWiye5kXP_53kNxaSs&callback=initMap&libraries=&v=weekly"
            defer
        ></script>
    </head>
    <body>
        {!! $map['html'] !!}
        <div id="directionsDiv"></div>
    </body>
</html>